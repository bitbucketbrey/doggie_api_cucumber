package Classes;

public class Pet {

    private int id;
    private String name;
    private String status;

    public Pet(int id, String name, String status)
    {
        this.id=id;
        this.name=name;
        this.status=status;
    }

    public int getId()
    {
        return this.id;
    }
}
