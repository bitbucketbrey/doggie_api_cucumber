package methods;

import java.util.Random;

public class GeneratePetName {

    public static String PetName()
    {
        String names[] ={"fluffy","tera","snoek","fishy","tiny","boere","sugar", "honey"};
        return names[new Random().nextInt(names.length)];
    }
}
