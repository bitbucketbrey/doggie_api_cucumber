import Classes.Pet;
import com.google.gson.Gson;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;

import static methods.GeneratePetName.PetName;
import static io.restassured.RestAssured.given;
public class AddPetDetails {

    Pet pet;
    String json;
    @Given("Pet Details")
    public void petDetails() {

        pet = new Pet(
                115530,
                PetName(),
                "available");

        System.out.println(PetName()+" entered.");
    }

    @And("serialize the Pet details")
    public void serializeThePetDetails() {

        Gson gson = new Gson();
        json = gson.toJson(pet);
    }

    @Then("Add the Pet details to the Pet store")
    public void addThePetDetailsToThePetStore() {

        Response response =
                given()
                        .contentType("application/json")
                        .body(json)
                        .when()
                        .post("https://petstore.swagger.io/v2/pet")
                        .then()
                        .statusCode(200)
                        .log().body()
                        .extract().response();

        String jsonString = response.asString();

        System.out.println(jsonString);
    }
}
