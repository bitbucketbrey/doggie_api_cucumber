import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;

import static io.restassured.RestAssured.given;
import static sun.net.InetAddressCachePolicy.get;
import org.json.JSONArray;
import org.json.JSONObject;

public class MyStepdefs {

    private Response response =null; //gets the response
    private int code=0; //gives response code
    private String dataPart=""; //gives response on the actual JSON body
    //private String resp = "";
    @Given("The endpoint for all Pets")
    public void theEndpointForAllPets() {
        response = RestAssured.get("https://petstore.swagger.io/v2/pet/findByStatus?status=available");
    }

    @And("I get the response code")
    public void iGetTheResponseCode() {
        given()
                .when()
                .get("https://petstore.swagger.io/v2/pet/findByStatus?status=available")
                .then()
                .statusCode(200);
    }

    @And("I get the test body")
    public void iGetTheTestBody() {
        dataPart = response.asString();
    }

    @Then("I search for doggie and id")
    public void iSearchForDoggieAndId() {

        JSONArray array = new JSONArray(dataPart);
        System.out.println("Name\tID");
        for(int i=0; i < array.length(); i++)
        {
            JSONObject object = array.getJSONObject(i);
            if(object.getString("name").equalsIgnoreCase("doggie"))//ID 12 does not exist
            {
                System.out.println(object.getString("name")+"\t"+object.getInt("id")+"\t"+object.getString("status"));//available
            }
        }

    }
}
