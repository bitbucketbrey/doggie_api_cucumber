import Classes.Pet;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import static methods.GeneratePetName.PetName;

public class FindPet {

    private Response response =null; //gets the response
    private int code=0; //gives response code
    private String dataPart=""; //gives response on the actual JSON body


    Pet pet;
    @Given("The Pet Identity Number")
    public void thePetIdentityNumber() {

        response = RestAssured.get("https://petstore.swagger.io/v2/pet/findByStatus?status=available");
        dataPart = response.asString();

        pet = new Pet(
                115530,
                PetName(),
                "available");
    }

    @Then("Search for the available Pet")
    public void searchForTheAvailablePet() {

        JSONArray array = new JSONArray(dataPart);
        System.out.println("Name\tID\t\tStatus");
        for(int i=0; i < array.length(); i++)
        {
            JSONObject object = array.getJSONObject(i);
            if(object.getInt("id")==115530)
            {
                System.out.println(object.getString("name")+"\t"+object.getInt("id")+"\t"+object.getString("status"));//available
            }
        }

    }
}
