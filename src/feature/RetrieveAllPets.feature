Feature: AllPetsRetrieved

  Scenario: I retrieve all available pets and confirm that the name 'doggie' with category id '12' is on the list
    Given The endpoint for all Pets
    And I get the response code
    And I get the test body
    Then I search for doggie and id